use chrono::{DateTime,Local, Timelike};
use log::{debug, error};
use std::{
    error::Error,
    io::{Write, ErrorKind},
    process::{Command, Stdio},
    thread,
    time::Duration, sync::{Arc, Mutex}, path::PathBuf, fs::File,
};

pub struct PersistenceHandle {
    should_run: Arc<Mutex<bool>>,
}

impl PersistenceHandle {
    pub fn kill(self) {
        *self.should_run.lock().unwrap() = false;
    }

    // Maybe handle errors and stuff from the persistence thread here
    // instead of letting the thread panic we could do more finegrained
    // communication with the main thread through this handle instead
    pub fn poll(&self) {}
}

pub enum ErrorStrategy {
    Ignore,
    Panic,
    Callback(Box<dyn FnOnce() -> () + std::marker::Send>),
}

pub enum PropagationFix {
    SlashTmp(PathBuf),
    None,
}

fn enforce_propagation_fix(propagation_fix: PropagationFix) -> Result<(), std::io::Error>{
    match propagation_fix {
        PropagationFix::None => {},
        PropagationFix::SlashTmp(path) => {
            if path.exists() {
                // kill this process completely
                // (in this case "this" process is the new one)
                panic!("Process is already running!");
            } else {
                File::create(path)?;
            }
        }
    }

    Ok(())
}

pub enum PersistenceError {
    TmpCreationError(std::io::Error),
}

// Shits the bed on laptop lid close (job doesnt die...) `enforce_propagation_fix()` fixes this
pub fn persist<Unit>(
    interval: Unit,
    error_strategy: ErrorStrategy,
    propagation_fix: PropagationFix,
) -> Result<PersistenceHandle, PersistenceError>
where
    Unit: Into<u64> + Clone + std::marker::Send + 'static,
{
    if let Err(e) = enforce_propagation_fix(propagation_fix) {
        return Err(PersistenceError::TmpCreationError(e));
    }

    let _command = std::env::current_exe().unwrap();
    let command = String::from("notify-send FAILED && touch /home/epos/IHAVEFAILED");


    // create bool for checking if should repeat wihch is shared across threads
    let should_run = Arc::new(Mutex::new(true));
    let dummy = should_run.clone();

    // Spawn new thread
    thread::spawn(move || {
        while *dummy.lock().unwrap() {
            //   insert at job
            let (scheduled_time, job_id) = match create_job(&command, interval.clone().into()) {
                Ok(t) => t,
                Err(e) => {
                    if e.is::<std::io::Error>() {
                        let io_error: &std::io::Error = e.downcast_ref().unwrap();

                        if let ErrorKind::NotFound = io_error.kind() {
                            error!("at tool was not found!");
                            panic!("at tool not found!");
                        }
                    }

                    error!("");
                    panic!("");
                },
            };

            let delta = (Local::now() - scheduled_time).num_seconds().unsigned_abs() as f32;

            // let user choose if those should be dont with a
            // offset from scheduled_time or dynamic like this
            // sleep for 3/4th off the time untill deadline
            let sleep_time: u32 = (delta * 0.95) as u32;

            debug!("{delta} seconds left untill {}", scheduled_time.format("%H:%M"));
            debug!("sleeping for {sleep_time} seconds");
            debug!("Waking up at {}", (Local::now() + chrono::Duration::from_std(Duration::new(sleep_time.into(), 0)).unwrap()).format("%H:%M:%S"));
            thread::sleep(Duration::new(sleep_time.into(), 0));

            if let Err(e) = kill_job(&job_id) {
                error!("{e}: failed to kill job...");
                // If this happened we are in BIG trouble.
                // Might need some way to call back to the user
                // that things are about to get wacky and that
                // they are at risk of duplicate processes
                match error_strategy {
                    ErrorStrategy::Ignore => {},
                    ErrorStrategy::Panic => panic!("Unsuccesfull in killing \"at\" job!"),
                    ErrorStrategy::Callback(c) => {
                        c();
                        break;
                    }
                }
            }
        }
    }).join().unwrap();

    Ok(PersistenceHandle {
        should_run
    })
}

fn create_job(
    command: &String,
    interval: u64,
) -> Result<(DateTime<Local>, String), Box<dyn Error>> {
    let interval = Duration::new(interval, 0);
    let intermediate_time = Local::now() + chrono::Duration::from_std(interval).unwrap();
    let intermediate_time = intermediate_time.with_second(0).unwrap();
    let time_string = format!("{}", intermediate_time.format("%H:%M"));

    let mut proc = Command::new("at")
        .arg(time_string)
        .stdin(Stdio::piped())
        .stderr(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;

    let proc_stdin = proc.stdin.as_mut().unwrap();
    proc_stdin.write_all(format!("{}\n", command).as_bytes())?;
    drop(proc_stdin);

    let byte_vec = proc.wait_with_output()?.stderr;
    let output = std::str::from_utf8(&byte_vec)?.replace("\n", " ");

    let items = output.split(" ").into_iter();
    let job = items.skip_while(|x| *x != "job").skip(1).next().unwrap();

    Ok((intermediate_time, job.to_string()))
}


fn kill_job(
    job: &str,
) -> Result<(), Box<dyn Error>> {
    let mut c = Command::new("atrm").arg(job.to_string()).spawn()?;

    debug!("Killing job {}", job);

    match c.wait() {
        Ok(_) => Ok(()),
        Err(e) => Err(Box::new(e)),
    }
}
