use rand::{thread_rng, distributions::Uniform, prelude::Distribution};
use at_persistence::{ErrorStrategy, PropagationFix};
use std::{fs, time::Duration, path::PathBuf};
use log::{info, error};


#[allow(dead_code)]
fn move_file() {
    let locations: Vec<&str> = vec![
        "wow",
        "cool",
        "why",
        "dude",
    ];

    let mut our_locations = locations.clone();
    let mut rng = thread_rng();

    loop {
        let current_location = std::env::current_exe().unwrap();
        info!("{current_location:?}");

        std::thread::sleep(Duration::new(3, 0));
        loop {
            if our_locations.is_empty() {
                error!("Ran out of locations to look...");
                return;
            }

            let gen = Uniform::from(0..our_locations.len());
            let i = gen.sample(&mut rng);

            let target_location = our_locations[i];
            our_locations.remove(i);

            info!("Trying location: {target_location}");

            match fs::rename(&current_location, target_location) {
                Ok(_) => {
                    fs::remove_file(&current_location).unwrap();
                    info!("Moved to {target_location}!");
                    break;
                },
                Err(e) => {
                    error!("{e}");
                },
            }
        }
    }
}

fn main() {
    env_logger::init();

    //move_file();

    let _h = at_persistence::persist(
        170u32,
        ErrorStrategy::Ignore,
        PropagationFix::SlashTmp(PathBuf::from("/tmp/PLACEHOLDER")),
    );
}
